package led

const (
	// ColorBlack is a constant for the color black.
	ColorBlack = 0x000000
	// ColorRed is a constant for the color red.
	ColorRed = 0xFF0000
	// ColorGreen is a constant for the color green.
	ColorGreen = 0x00FF00
	// ColorBlue is a constant for the color blue.
	ColorBlue = 0x0000FF
	// ColorPurple is a constant for the color purple.
	ColorPurple = 0xFF00C8
	// ColorViolet is a constant for the color violet.
	ColorViolet = 0xC800FF
	// ColorYellow is a constant for the color yellow.
	ColorYellow = 0xFFC800
	// ColorOrange is a constant for the color orange.
	ColorOrange = 0xFF6400
	// ColorWhite is a constant for the color white.
	ColorWhite = 0xFFFFFF
)

// RGBToBin converts a set of RGB values to a binary number that
// can directly be used by the underlying driver.
func RGBToBin(r uint32, g uint32, b uint32) uint32 {
	return uint32(((r & 0xFF) << 16) + ((g & 0xFF) << 8) + (b & 0xFF))
}
