// +build arm

package led

import (
	"log"
	"os"

	ws2811 "github.com/rpi-ws281x/rpi-ws281x-go"
)

// Configure LED driver.
const (
	// dmaNum     = 5
	gpioPin    = 18
	brightness = 255
	ledCount   = 1
)

// Pointer to the object used to drive the Neopixel.
var led *ws2811.WS2811

// A channel to receive OS signals for resetting the LED driver.
var signals chan os.Signal

// init will configure the hardware LED driver.
func init() {
	// Inidicate that the hardware driver is being used.
	log.Println("LED: using hardware driver")

	// Configure the LED driver.
	opt := ws2811.DefaultOptions
	// opt.DmaNum = dmaNum
	opt.Channels[0].GpioPin = gpioPin
	opt.Channels[0].Brightness = brightness
	opt.Channels[0].LedCount = ledCount

	// Create configuration for the LED driver.
	var err error
	led, err = ws2811.MakeWS2811(&opt)
	if err != nil {
		log.Fatalf("error: configuring hardware driver failed: %s", err)
	}

	// Initialize hardware driver.
	err = led.Init()
	if err != nil {
		log.Fatalf("error: initializing hardware driver failed: %s", err)
	}
}

// render will flush the color to the LED.
func render(color uint32) {
	// Apply color to all LEDs.
	for i := 0; i < len(led.Leds(0)); i++ {
		led.Leds(0)[i] = color
	}

	// Render colors to LED.
	if err := led.Render(); err != nil {
		log.Fatalf("error: rendering colors failed: %s", err)
	}
}

// Reset will disable all LEDs and reset the driver.
func Reset() {
	// Disable all LEDs.
	Static(ColorBlack)

	// Reset driver.
	led.Fini()
}
