package tmpstore

import (
	"gitlab.com/sdu-labcloud/node-linux-go/internal/labcloud"
)

// Peripherals describes devices connected to the node
type Peripherals struct {
	LockType string `json:"lock_type"`
}

// CachedData describes the data store in the tmpstore.
type CachedData struct {
	Session     labcloud.Session `json:"session"`
	Peripherals Peripherals      `json:"peripherals"`
}
