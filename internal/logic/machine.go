package logic

import (
	"time"

	"gitlab.com/sdu-labcloud/node-linux-go/internal/labcloud"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/led"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/system"
)

// AwaitMachineAssignment ensures that the node
// has a machine assigned to it and blocks the
// the process until this is the case.
func AwaitMachineAssignment(mutexes *system.Mutexes, node *labcloud.Node) labcloud.Machine {
	// While there is no machine assigned to the node watch the API.
	for machineID == "" {
		mutexes.Node.Lock()
		updatedNode := labcloud.ReadNode(node.ID)

		// Only update local state if the request was successful.
		if updatedNode.ID != "" {
			*node = updatedNode
			machineID = updatedNode.Machine
		}
		mutexes.Node.Unlock()

		led.Static(led.ColorOrange)

		if machineID != "" {
			initialized = true
			break
		}

		time.Sleep(time.Second)
	}

	machine := labcloud.ReadMachine(machineID)
	for machine.ID == "" {
		time.Sleep(time.Second)
		machine = labcloud.ReadMachine(machineID)
	}

	return machine
}

// CheckMachineStatus continously polls the machine status.
// This allows admins to change the machine status through the web interface.
func CheckMachineStatus(mutexes *system.Mutexes, machine *labcloud.Machine) {
	for {
		mutexes.Machine.Lock()
		currentStatus := labcloud.ReadMachine(machine.ID).Status
		if machine.Status != currentStatus {
			machine.Status = currentStatus
		}
		mutexes.Machine.Unlock()
		time.Sleep(time.Second * 5)
	}
}
