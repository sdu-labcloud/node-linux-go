package system

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/inconshreveable/go-update"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/labcloud"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/led"
)

// CommitSHA is the commit SHA against which this application was built.
var CommitSHA string

// CheckUpdate will check the latest commit against the current commit. It will run the Update function if the commits are different.
func CheckUpdate(mutexes *Mutexes, machine *labcloud.Machine, node *labcloud.Node) {
	log.Println("Application commit SHA: " + CommitSHA)

	for {
		// Update the node object
		mutexes.Node.Lock()
		*node = labcloud.ReadNode(node.ID)

		// Check newest application on specified channel
		application := labcloud.ReadLatestApplication(node.ApplicationName, node.ReleaseChannel)
		mutexes.Node.Unlock()

		if CommitSHA != "dev" && application.CommitSHA != "" && application.CommitSHA != CommitSHA {
			log.Println("Updating to version: " + application.CommitSHA)
			mutexes.Update.Lock()

			// Set machine status during update.
			if machine.ID != "" {
				led.Static(led.ColorOrange)
				updatedMachine := labcloud.UpdateMachineStatus(machine.ID, "maintenance")

				if updatedMachine.ID != "" {
					mutexes.Machine.Lock()
					*machine = updatedMachine
					mutexes.Machine.Unlock()
				}
			}

			// Download new binary
			resp, err := http.Get(application.DownloadHref)
			if err != nil {
				// Abort update if an error occurs while attempting to download it.
				mutexes.Update.Unlock()
			} else {
				// Apply update if new binary can be downloaded.
				err = update.Apply(resp.Body, update.Options{})
				resp.Body.Close()

				if err != nil {
					rerr := update.RollbackError(err)
					mutexes.Update.Unlock()
					if rerr != nil {
						log.Fatalf("error: failed to rollback from bad update: %v", rerr)
					}
				}

				log.Println("Restarting to apply update!")
				os.Exit(0)
			}
		}
		time.Sleep(time.Second * 5)
	}
}
