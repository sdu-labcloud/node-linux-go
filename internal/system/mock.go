// +build !arm

package system

import (
	"log"
	"os"
	"strings"

	"github.com/joho/godotenv"
)

// Serial is a mock for reading the systems serial number.
// It will return the serial number specified in the
// MOCK_SERIAL environment variable in the ".env" file.
func Serial() string {
	// Load values from ".env" file.
	err := godotenv.Overload()
	if err != nil {
		log.Fatalf("error: loading .env file failed: %s", err)
	}

	// Return value of mock environment variable if present.
	serial := strings.TrimSpace(os.Getenv("MOCK_SERIAL"))
	if len(serial) != 0 {
		return serial
	}

	// Return well-known string if serial number cannot be loaded.
	return "NO_SERIAL"
}
